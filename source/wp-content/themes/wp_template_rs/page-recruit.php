<?php get_header(); ?>
	<!--<div class="reseed_col_l_842"><!-- begin reseed_col_l_842 -->
	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9">   
		<h1 class="headline">採用情報</h1>
		<div class="padding_0_10">
			<table id="comp_outline" class="table" width="100%">
	     		<tr>
	     			<th width="24%" class="vert_middle">雇用形態</th>
	     			<td>正社員・アルバイト・インターン
	     				（その他希望により応相談）</td>
	     		</tr>
	     		<tr>
	     			<th class="vert_middle">募集要項</th>
	     			<td>
	     				<ul class="list-unstyled">
	     					<li class="margin_ver10">
	     						<ul class="list-unstyled margin_ver10">
	     							<li class="fontbold">正社員 ＜雇用保険、労災、社会保険、年金、完備＞</li>
	     							<li>WEBアカウントプランナー　220,000円〜400,000円</li>
	     							<li>総合職＜海外交渉、仕入れ交渉、マーケティング、広告運用＞　200,000円</li>
	     							<li>WEBデザイナー、ディレクター、コーダー,ショップ運営　200,000円</li>
	     							<li>※TOEIC800以上、営業、デザイナー等の3年以上の経験者は優遇します</li>
	     						</ul>
	     					</li>
	     					<li class="margin_ver10">
	     						<ul class="list-unstyled">
	     							<li class="fontbold">契約社員 ＜3ヶ月ごとの更新、雇用保険、労災完備＞</li>
	     							<li>総合職[コールセンター管理、海外交渉、顧客対応]</li>
	     							<li>日給8,000円～12,000円</li>
	     						</ul>
	     					</li>
	     					<li class="margin_ver10">
	     						<ul class="list-unstyled">
	     							<li class="fontbold">アルバイト ＜雇用保険労災完備＞</li>
	     							<li>時給850円～1,200円</li>
	     							<li>総合職＜コールセンター管理、事務、海外交渉、顧客対応＞</li>
	     							<li>DTPオペレーター＜紙袋入稿デザイン対応、パッケージデザイン制作＞</li>
	     						</ul>
	     					</li>
	     				</ul><!-- end list-unstyled -->
     			  </td>
				</tr>
	     		<tr>
	     			<th class="vert_middle">勤務地</th>
	     			<td>大阪市淀川区西中島5-6-9 新大阪第一ビル 3階</td>
	     		</tr>
	     		<tr>
	     			<th class="vert_middle">休日休暇</th>
	     			<td>週休2日（土・日）</td>
	     		</tr>
	     		<tr>
	     			<th class="vert_middle">勤務時間</th>
	     			<td>9時～18時　休憩1.5時間</td>
	     		</tr>
	     		<tr>
	     			<th class="vert_middle">待遇</th>
	     			<td>
						<ul class="list-unstyled">
	     					<li>交通費：　　　　＜25,000円まで支給＞</li>
   					        <li>家賃手当て：　　＜半径3キロ以内に移住の方は10,000円家賃手当て＞</li>
	     					<li>半期決算賞与：　＜営業利益の10%を分配＞</li>
	     					<li>昇給、昇格：　　＜1月、4月、7月、10月 査定＞</li>
	     					<li>社会保険制度：　＜有り＞</li>
	     				</ul><!-- end list-unstyled -->
	     			</td>
	     		</tr>
	     	</table><!-- end comp_outline -->
		</div><!-- end padding_0_10 -->
		<h1 class="headline">職種別求人詳細</h1>
	   	<h2 class="sub_headline">Webシステムエンジニア）</h2>
		<div class="padding_0_10">
			<table id="comp_outline" class="table" width="100%">
	     		<tr>
	     			<th width="24%" class="vert_middle">募集職種</th>
	     			<td>Webシステムエンジニア</td>
	     		</tr>
	     		<tr>
	     			<th class="vert_middle">仕事内容</th>
	     			<td>
						<ul class="list-unstyled">
	     					<li class="margin_ver10">
	     						<ul class="list-unstyled margin_ver10">
	     							<li class="fontbold">◆クライアント案件のシステム開発業務</li>
									<li>お客様からのご依頼に応じて、ニーズに合わせたWebシステム開発を行います。<br>
									デザイナーや海外開発チームと連携し、クライアントの要望をカタチにして頂きます。</li>

									<li class="fontbold margin_ver10">◆自社サービスの開発業務</li>
									<li>自社で展開しているサービスのバックボーン開発を行います。<br>
									オペレーターやマーケッターの日々の運用効率を上げるための、自社専用システムの開発を行って頂きます。</li>

									<li class="fontbold margin_ver10">◆社内ツール、社内システム開発業務</li>
									<li>社内の業務を効率化させるため、社内のやり方に沿った、最適なシステム開発を行って頂きます。</li></ul>
								</li>
						</ul><!-- end list-unstyled -->
					</td>
	     		</tr>
	     		<tr>
	     			<th class="vert_middle">応募資格</th>
	     			<td>
	     				<ul class="list-unstyled">
	     					<li class="margin_ver10">
	     						<ul class="list-unstyled margin_ver10">
	     							<li class="fontbold">応募条件</li>
	     							<li>■学歴不問</li>
	     						</ul>
	     					</li>
	     					<li class="margin_ver10">
	     						<ul class="list-unstyled margin_ver10">
	     							<li class="fontbold">【必須条件】</li>
	     							<li>■社会人経験がある方</li>
	     							<li>■システムエンジニアとしての実務経験がある方</li>
									<li>〈必須スキル〉<br>
									言語： PHP, HTML5+CSS3, SQL,
									フレームワーク: Zend Framework, jQuery,
									データベース: MySQL
									開発環境：Apache, Git(BitBucket), Amazon AWS<br>
									- Web系の実務開発経験 3年<br>
									- PHPで実務開発経験 3年</li>

									<li>◎下記のような知識があればシステムエンジニアの実務経験がなくても歓迎します！</li>
									<li>■web・メール・Word、Excelなどの、基本的な知識がある方</li>
									<li>■php、MySQL、javaScript言語を使用出来る方</li>
	     						</ul>
	     					</li>
	     					<li class="margin_ver10">
	     						<ul class="list-unstyled">
	     							<li class="fontbold">【歓迎スキル】</li>
	     							<li>■下記使用できる方
									- Smarty
									- Yii Framework
									- Node.js
									- Python
									- MongoDB</li>
									<li>■web業界での経験や知識がある方</li>
									<li>■スケジュール管理・プロジェクト完遂能力がある方</li>

									<li>※英語によるコミュニケーション・スキルがある方(必須ではありません)
									</li>
	     						</ul>
	     					</li>
	     				</ul><!-- end list-unstyled -->
     			  </td>
				</tr>
	     		<tr>
	     			<th class="vert_middle">求める人物像</th>
	     			<td>
						<ul class="list-unstyled">
	     					<li class="margin_ver10">
	     						<ul class="list-unstyled margin_ver10">
	     							<li>- ウェブサービスやスタートアップの価値が好きで新しいことにドンドン挑戦してくれる方</li>
									<li>- 積極的にコミュニケーションをとれる方</li>
									<li>- 勉強好きな方</li>
									<li>- ロジカルシンキング＆仮説が得意な方</li>
									<li>- ポジティブに、変化を楽しみながら仕事ができる方</li>
								</ul>
							</li>							
							<li>
								<ul class="list-unstyled margin_ver10">
									<li class="margin_ver10">
										<ul class="list-unstyled">
											<li class="fontbold">
											ポイント：</li>
											<li>- 一人ひとりの裁量が大きく、幅広い業務に携わることができます。</li>
											<li>- 自社運営のWebサービスを行っており、自社内で腰を据えて仕事を進めることが可能です。</li>
											<li>- 意欲があれば、挑戦できる環境です。提案があれば、積極的に行なっていただくことができます。</li>
											<li>- 成長企業にて自分の仕事がダイレクトに会社の成長に反映されます。</li>
											<li>- 自ら学んだ技術を積極的に活かすことができる環境です。</li>
										</ul>
									</li>
								</ul>
							</li>
						</ul><!-- end list-unstyled -->
					</td>
	     		</tr>
	     		<tr>
	     			<th class="vert_middle">給与</th>
	     			<td>250,000円（賞与２回有り）<br>※上記はあくまで最低給与額です。経験・能力を考慮した上で、最終決定致します。</td>
	     		</tr>
	     	</table><!-- end comp_outline -->
	    </div><!-- end padding_0_10 -->
	    <h2 class="sub_headline margin_ver10">SEMコンサルタント</h2>
		<div class="padding_0_10">
	     	<table id="comp_outline" class="table" width="100%">
	     		<tr>
	     			<th width="24%" class="vert_middle">募集職種</th>
	     			<td>SEMコンサルタント</td>
	     		</tr>
	     		<tr>
	     			<th class="vert_middle">仕事内容</th>
	     			<td>
						<ul class="list-unstyled">
	     					<li class="margin_ver10">
	     						<ul class="list-unstyled margin_ver10">
	     							<li class="fontbold">◆アカウントコンサルティング業務</li>
									<li>電話によるご案内と、当社が運営する媒体からくるお客様へのご提案業務です。</li>

									<li class="fontbold margin_ver10">◆Webサイト運用・分析・改善提案業務</li>
									<li>・Web解析、競合分析等を基にした改善提案業務<br>・制作スタッフへのディレクション業務など。</li>

									<li class="fontbold margin_ver10">◆定期的なコンサルティング業務</li>
									<li>担当顧客を定期的に訪問し、分析改善案を提出・Webの効果を最大化する お手伝いを行って頂きます。</li>
									<li class="fontbold margin_ver10">◆レポート・分析資料作成業務</li>
									<li>Web解析、SEO施策、リスティング広告等の運用における数値を抽出・分析し、Web戦略の立案や費用対効果の向上のための資料作成。</li>
									<li class="fontbold margin_ver10">◆リスティング広告の運用・検証・改善提案業務</li>
									<li>出稿キーワードや広告文案の提案、運用の検証結果に基づいた改善等を行います。</li>
								</ul>
							</li>
						</ul><!-- end list-unstyled -->
					</td>
	     		</tr>
	     		<tr>
	     			<th class="vert_middle">応募資格</th>
	     			<td>
	     				<ul class="list-unstyled">
	     					<li class="margin_ver10">
	     						<ul class="list-unstyled margin_ver10">
	     							<li class="fontbold">応募条件</li>
	     							<li>■学歴不問</li>
	     						</ul>
	     					</li>
	     					<li class="margin_ver10">
	     						<ul class="list-unstyled margin_ver10">
	     							<li class="fontbold">【必須条件】</li>
	     							<li>■社会人経験(法人営業経験2年以上、または個人営業経験5年以上)ある方</li>
	     						</ul>
	     					</li>
	     					<li class="margin_ver10">
	     						<ul class="list-unstyled">
	     							<li class="fontbold">【歓迎スキル】</li>
	     							<li>■web業界での経験や知識がある方</li>
									<li>■スケジュール管理・プロジェクト完遂能力がある方</li>
									<li>■コンサルティング業務経験がある方(業種不問)</li>

									<li>※英語によるコミュニケーション・スキルがある方(必須ではありません)
									</li>
									<li>※テレフォンオペレーター経験がある方歓迎◎</li>
	     						</ul>
	     					</li>
	     				</ul><!-- end list-unstyled -->
     			  </td>
				</tr>
	     		<tr>
	     			<th class="vert_middle">求める人物像</th>
	     			<td>
						<ul class="list-unstyled">
	     					<li class="margin_ver10">
	     						<ul class="list-unstyled margin_ver10">
	     							<li>明るく感情豊かなコミュニケーションが取れる方</li>
									<li>自分で考え、一人でも業務を進められる方</li>
								</ul>
							</li>
						</ul><!-- end list-unstyled -->
					</td>
	     		</tr>
	     		<tr>
	     			<th class="vert_middle">給与</th>
	     			<td>250,000円（賞与２回有り）<br>※上記はあくまで最低給与額です。経験・能力を考慮した上で、最終決定致します。<br>※上記給与に加え、個人の売上金額に応じたインセンティブを毎月支給致します。</td>
	     		</tr>
	     	</table><!-- end comp_outline -->
	   </div><!-- end padding_0_10 -->	
	</div><!-- end col -->
<?php get_footer(); ?>
