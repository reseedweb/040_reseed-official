<?php get_header(); ?>
<!--<div class="reseed_col_l_842"> begin reseed_col_l_842 -->	
<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9"><!-- begin col -->	
	<div class="clearfix cf margin_btm_30">
		<p class="saiyou_text1">株式会社RESEEDでは、中途・2016年度新卒の採用募集を行っております。</p>
		<div class="left saiyou mr20 mb20">
			<p class="saiyou_text">中途採用の情報はこちら</p>
			<a href="http://reseed-recruit.com/"><img src="<?php bloginfo('template_url');?>/img/btn1.jpg" alt="中途採用情報はこちら" /></a>
		</div><!-- end saiyou -->	
		<div class="left saiyou mb20">
			<p class="saiyou_text">新卒採用の情報はこちら</p>
			<div class="clearfix cf">
				<a class="top-link" href="http://shinsotsu.reseed-recruit.com/"><img class="left top-img" src="<?php bloginfo('template_url');?>/img/btn2.jpg" alt="新卒採用情報はこちら" /></a>
				<a class="top-link" href="https://job.mynavi.jp/16/pc/search/corp202909/outline.html" target="_blank"><img class="left ml10 top-job top-img" src="https://job.mynavi.jp/conts/kigyo/2016/logo/banner_entry_160_45.gif" width="238px" alt="マイナビ2016" border="0" /></a>
			</div>
		</div><!-- end saiyou -->
	</div><!-- end div -->

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			<div class="media margin_btm_10">
				<p class="pull-right"><a href="<?php bloginfo('url');?>/webmarketing"><img class="media-object mousehover" src="<?php bloginfo('template_url');?>/images/top_media1.jpg" alt="<?php bloginfo('name'); ?>" /></a></p>
				<div class="media-body">
					<h3 class="media-heading font80 my_col_red fontbold">Web Marketing</h3>
					<h4 class="fontbold font90">ウェブマーケティング事業</h4>
					<a class="media_link" href="<?php bloginfo('url');?>/webmarketing" title="ウェブマーケティング事業紹介">詳細をみる <i class="icon-chevron-right"></i></a>
				</div><!-- end media-body -->
			</div><!-- end media -->
			<p>SEO・SEMの独自ノウハウの基としたマーケティング事業を展開。<br />多種多様な業界のサイト運用実績が300件、5年以上の豊富な経験をもつ自社のSEMエキスパートによるサービスをご提供しております。</p>
		</div><!-- end col -->

		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			<div class="media margin_btm_10">
				<p class="pull-right"><a href="<?php bloginfo('url');?>/globalmarketing"><img class="media-object mousehover" src="<?php bloginfo('template_url');?>/images/top_media2.jpg" alt="<?php bloginfo('name'); ?>" /></a></p>
				<div class="media-body">
					<h3 class="media-heading font80 my_col_red fontbold">Global Markething</h3>
					<h4 class="fontbold font90">グローバルマーケティング事業</h4>
					<a class="media_link" href="<?php bloginfo('url');?>/globalmarketing" title="グローバルマーケティング事業紹介">詳細をみる <i class="icon-chevron-right"></i></a>
				</div><!-- end media-body -->
			</div><!-- end media -->
			<p class="margin_btm_30">各国でのクライアントの関心度・認知度をはじめ、ウェブ状況などを調査＆コンサルティングサービスを展開。<br />東南アジアを中心とした企業と日本国内の工場を連携することで販促物、消耗品、デザイン物などを高品質・安価でご購入できる仕組みをご提供しております。</p>
		</div><!-- end col -->
	</div><!-- end row -->
	
	<div class="clearfix"></div>

	<h2 class="headline">最新情報</h2>
	<table id="news_table" width="100%">		
		<!--<tr>
			<td width="7%"><span class="my_label_orange">お知らせ</span></td>
			<td>2013/09/08　業績好調、業務拡大につき、事務員１名を募集。 <a href="http://next.rikunabi.com/company/cmi3246718001/nx1_rq0009993943/?fr=cp_s00700&list_disp_no=1" target="_blank"　a="採用情報ページへ">詳細はこちら  <i class="icon-chevron-right"></i></a></td>
		</tr>-->
		<tr>
			<td width="7%"><span class="my_label_orange">お知らせ</span></td>
			<td>2015/4/23　2015年5月2日（土）～5月6日（水）まで、GW休暇とさせていただきます。</td>
		</tr>
		   <tr>
			<td width="7%"><span class="my_label_orange">お知らせ</span></td>
			<td>2015/4/20　<a href="http://shinsotsu.reseed-recruit.com/">2016年度新卒採用サイト</a>をオープンしました。</td>
		</tr>
		<tr>
			<td width="7%"><span class="my_label_orange">お知らせ</span></td>
			<td>2014/12/19　2014年12月27日（土）～2015年1月4日（日）まで、冬季休暇とさせていただきます。</td>
		</tr>
		<tr>
			<td><span class="my_label_green">新着情報</span></td>
			<td><a href="info.html">2014/011/06　事務所移転のお知らせ</a></td>
		</tr>
		<tr>
			<td><span class="my_label_blue">事業情報</span></td>
			<td>2014/01/30　ベトナム・オフィスを開設しました。</td>
		</tr>
		<tr>
			<td><span class="my_label_green">新着情報</span></td>
			<td>2013/09/30　おかげさまで取引先クライアント数500社を突破しました。</td>
		</tr>
		<tr>
			<td width="7%"><span class="my_label_orange">お知らせ</span></td>
			<td>2013/09/04　RESEED公式サイトをリニューアルしました。</td>
		</tr>
		<!--<tr>
			<td><span class="my_label_blue">事業情報</span></td>
			<td>2012/06/01　Google認定パートナー、Yahooリスティング広告正規代理店となりました。</td>
		</tr>
		<tr>
			<td><span class="my_label_blue">事業情報</span></td>
			<td>2013/02/01　本社を大阪市東中島1-17-5ステュディオ新大阪に移転しました。</td>
		</tr>-->
	</table><!-- end news_table -->

	<div height="30">&nbsp;</div>
	<h2 class="headline top-h2">リスティング広告運用代行と戦略的ウェブコンサルティングなら株式会社レシードへ</h2>
	<div class="padding_0_10">
		<p>株式会社レシード〔RESEED〕はWEBマーケティング・開発・IT支援などを提案するリスティング広告、PPC広告運用に特化したWEB戦略構築コンサルティングファームです。</p>
	</div><!-- end padding_0_10 -->
	<div style="height:10px"></div>
	<ul class="top_list1 margin_top_20">
		<li>SEM、リスティング広告、Yahooリスティング、adwords広告、解析運用</li>
		<li>グーグル検索3000検索未満を得意とするロングテールSEO</li>
		<li>PHPによるウェブシステム開発、デザイン作成</li>
	</ul><!-- end top_list1 -->
	<div class="padding_0_10 margin_btm_30">
		<p>ITを活用したサポート支援をワンストップでご提供しております。株式会社RESEEDでは小額から始めるウェブ戦略の総合構築支援を得意としており、30業種を超える多業種における企業様での成功事例にからなる、成功法則から失敗法則まで、豊富なマーケティングノウハウを蓄えております。</p>
	</div><!-- end padding_0_10 -->
	<h2 class="headline">レシードが手がけた成功事例</h2>
	<ul class="top_list1 margin_btm_20">
		<li>印刷、販促商材のプロモーション事業のウェブ事業を立ち上げてゼロから年商1億円構築</li>
		<li>売り上げ0円の設立間もない士業の戦略構築を行い、月間安定売り上げ200万構築</li>
		<li>保守・メンテナンス設備業の企業様にて、下請け100%の状態から直接取引企業が年間60社にのぼる販路及び売上拡大に成功</li>
	</ul><!-- end top_list1 -->
	<img src="<?php bloginfo('template_url');?>/images/top_bnr1.jpg" class="margin_20_10 top-contact" alt="最大限の効果訴求施策・リスティング広告・PPC広告の運用" />
</div><!-- end reseed_col_l_842 -->
<?php get_footer(); ?>