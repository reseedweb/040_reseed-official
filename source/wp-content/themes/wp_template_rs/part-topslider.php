<div class="hidden-xs">
    <div id="myCarousel" class="carousel slide"><!-- begin carousel -->		
		<ol class="carousel-indicators"><!-- end carousel-indicators -->
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		</ol><!-- end carousel-indicators -->
		<div class="container"><!-- begin container -->
			<div class="carousel-inner"><!-- begin carousel-inner -->
				<div class="item active">
					<img src="<?php bloginfo('template_url');?>/images/slide1.jpg" alt="スライド１" />
				</div>
				<div class="item">
					<img src="<?php bloginfo('template_url');?>/images/slide2.jpg" alt="スライド２" />
				</div>
				<div class="item">
					<img src="<?php bloginfo('template_url');?>/images/slide3.jpg" alt="スライド３" />
				</div>
		    </div><!-- end carousel-inner -->
		</div><!-- end container -->
    </div><!-- end carousel -->
</div><!-- end responsible utility -->
 
	
