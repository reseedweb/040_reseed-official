<!DOCTYPE html>
<html>
	<head>
        <!-- meta -->        
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />         
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="株式会社RESEED　Rseed　レシード　リスティング広告　PPC広告　大阪" />
		<meta name="description" content="大阪のPPC広告、リスティング広告、web戦略構築に特化したwebマーケティング会社になります。" />
		<meta name="publisher" content="Reseed Co">
		
        <!-- title -->
        <title><?php if(is_home()){ echo bloginfo("name"); echo " | "; echo bloginfo("description"); } else { echo wp_title(" | ", false, right); echo bloginfo("name"); } ?> </title>
        <meta name="robots" content="noindex,follow,noodp" /> 

		<!-- Fav and touch icons -->
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php bloginfo('template_url'); ?>/images/favicon144.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php bloginfo('template_url'); ?>/images/favicon114.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php bloginfo('template_url'); ?>/images/favicon72.png">		
		<link rel="icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" type="image/x-icon">	       
		
		<!-- Open Graph Meta -->
		<meta property="og:title" content="株式会社レシード"/>
		<meta property="og:description" content="大阪のPPC広告、リスティング広告、web戦略構築に特化したwebマーケティング会社になります。"/><!-- 300文字まで -->
		<meta property="og:url" content="http://reseed-s.com"/>
		<meta property="og:image" content="http://reseed-s.com/images/logo.jpg"/>
		<meta property="og:type" content="#"/>
		<meta property="og:site_name" content="株式会社レシード公式サイト"/>

		<!-- Boostrap -->
		<link href="<?php bloginfo('template_url'); ?>/assets/bootstrap/css/bootstrap.css" rel="stylesheet">				
			
		<!-- Fontawesome -->
		<link href="<?php bloginfo('template_url'); ?>/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
			
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/respond.min.js"></script>
        <![endif]--> 
		
		<!-- JS -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"></script>						                               
		<script src="<?php bloginfo('template_url'); ?>/assets/modernizr/modernizr.custom.66393.js"></script>		
		<script src="<?php bloginfo('template_url'); ?>/assets/mustache/mustache.js" type="text/javascript" ></script>		
		<script src="<?php bloginfo('template_url'); ?>/assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/reseed.js" type="text/javascript"></script>
		
		<!-- CSS -->		
		<link href="<?php bloginfo('template_url'); ?>/style.css?<?php echo md5(date('l jS \of F Y h:i:s A')); ?>" rel="stylesheet" />						
        <?php wp_head(); ?>
    </head>
    <body>
		<header id="top_header" class="navbar"><!-- begin header -->
			<div class="container"><!-- begin container -->
				<h1 id="main_catch" class="text_shadow_b hidden-xs">WEBマーケティング、EC収益化戦略構築、グローバルマーケティングならレシード</h1>
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><i class="icon-reorder"></i></button>
				  <a class="brand" href="<?php bloginfo('url');?>/" title="株式会社レシード・公式サイト"><img src="<?php bloginfo('template_url');?>/images/logo.jpg" alt="株式会社レシード・公式サイト" /></a>
				</div><!-- end navbar-header -->
				<?php get_template_part('part','nav'); ?>	
			</div><!-- end container -->
		</header><!-- end header -->
		
		<?php if(is_home()) : ?>
			<?php get_template_part('part','topslider'); ?>	
		<?php else :?>			
			<?php get_template_part('part','topcontent'); ?>	
		<?php endif; ?>		
		<div class="container wrapper"><!-- begin container -->	
			<div class="row"><!-- end row -->
				<div id="left_column"><!-- end left_column -->