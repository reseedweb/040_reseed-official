<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
	<div id="side_tel" class="margin_btm_20">
		<p class="side_tel center_text"><a href="tel:0648624164"><i class="icon-mobile-phone"></i> <span itemprop="telephone"><strong>06-4862-4164</strong></span></a><br/></p>
		<p class="font90 center_text">受付： 平日 9:00 - 18:00</p>
	</div><!-- end side_tel -->
	
	<div class="align-pc"><img src="<?php bloginfo('template_url');?>/images/side_bnr1.jpg" class="margin_btm_10" alt="認定パートナー取扱店" /></div>
	<p class="font80 margin_btm_20">株式会社レシードはYahooリスティング広告正規代理店／Google認定パートナー取扱店です</p>
	<h3 class="headline">運営メディア</h3>
	<div class="align-pc"><a href="http://sem-consul.com" title="semコンサル.comへ" target="_blank"><img src="<?php bloginfo('template_url');?>/images/side_bnr2.jpg" class="margin_btm_10 mousehover" alt="リスティング広告・PPC広告運用のエキスパート" /></a></div>
	<p class="font80 margin_btm_20">リスティング広告・PPC広告運用のエキスパート</p>
	
	<div class="margin_btm_20">
		<h3 class="headline">メディア掲載</h3>
		<p class="font80 margin_btm_20">・代表大江のインタビューが<a class="font120 margin_btm_20" href="http://kigyotv.jp/interview/reseed/" target="_blank">「起業tv」</a>に掲載されました.</p>
	</div>
	
	<?php if(is_home()) : ?>
		<div class="margin_btm_20">
			<h3 class="headline">採用情報</h3>
			<div class="align-pc">
				<a href="http://reseed-recruit.com/" title="中途採用サイト" target="_blank"><img src="<?php bloginfo('template_url');?>/img/btn4.jpg" class="margin_btm_10 mousehover" alt="中途採用サイト" /></a>
				<a href="http://shinsotsu.reseed-recruit.com/" title="新卒採用サイトへ" target="_blank"><img src="<?php bloginfo('template_url');?>/img/btn3.jpg" class="margin_btm_10 mousehover" alt="新卒採用サイト" /></a>
				<!-- Begin mynavi Navi Link -->
				<a href="https://job.mynavi.jp/16/pc/corpmypage/loginMypage/index?corpId=202909" target="_blank"><img src="https://job.mynavi.jp/conts/kigyo/2016/logo/banner_entry_130_130.gif" alt="マイナビ2016" width="261" border="0"></a>
			</div>			
		</div>
	<?php endif; ?>		
	
	<h3 class="headline">企業情報</h3>
	<div class="align-pc">
		<a href="<?php bloginfo('url');?>/company" title="会社概要"><img src="<?php bloginfo('template_url');?>/images/side_bnr3.jpg" class="margin_btm_20 mousehover" alt="会社概要" /></a>
		<a href="<?php bloginfo('url');?>/staff" title="社員紹介"><img src="<?php bloginfo('template_url');?>/images/side_bnr4.jpg" class="margin_btm_20 mousehover" alt="社員紹介" /></a>
		<a href="<?php bloginfo('url');?>/recruit" title="採用情報"><img src="<?php bloginfo('template_url');?>/images/side_bnr5.jpg" class="margin_btm_20 mousehover" alt="採用情報" /></a>
		<a href="http://ameblo.jp/ohenaga/" target="_blank" title="代表ブログ"><img src="<?php bloginfo('template_url');?>/images/side_bnr6.jpg" class="margin_btm_20 mousehover" alt="代表ブログ" /></a>	
	</div>	
</div><!-- col -->