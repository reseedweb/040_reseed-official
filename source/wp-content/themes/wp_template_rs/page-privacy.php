<?php get_header(); ?>
	<!--<div class="reseed_col_l_842"><!-- begin reseed_col_l_842 -->
	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9" itemscope itemtype="http://schema.org/Organization">		
		<h1 class="headline">プライバシーポリシー</h1>
		<div class="padding_0_10">
			<p>
				株式会社RESEEDでは利用者の皆様が安心してご利用頂けるよう最低限の個人情報を提供頂いております。<br>
				弊社ではご提供頂いた個人情報の保護について最大限の注意を払っています。 <br>
				弊社の個人情報保護についての考え方は以下の通りです。<br>
			</p>
		</div><!-- end padding_0_10 -->
		
		<h2 class="sub_headline">1. 個人情報保護</h2>
	   	<div class="padding_0_10 margin_btm_10">
			■ 弊社では会員様により登録された個人及び団体や法人の情報については、弊社において最先端の機能やサービスを開発・提供するためにのみ利用し、会員個人情報の保護に細心の注意を払うものとします。<br>
			■ このプライバシーポリシーの適用範囲は、弊社で提供されるサービスのみであります。(範囲は下記、第1項に規定)<br>
			■ 本規約に明記された場合を除き、目的以外の利用は致しません。(目的は下記、第2項に規定)<br>
			■ 本規約に明記された場合を除き、第三者への開示は致しません。(管理は下記、第2項に規定)<br>
			■ その他本規約に規定された方法での適切な管理を定期的に行います。<br>
			■ 弊社は利用者の許可なくして、プライバシーポリシーの変更をすることができます。弊社が、個人情報取得内容の変更・利用方法の変更・開示内容の変更等をした際には、利用者がその内容を知ることができるよう、弊社ホームページのお知らせに公開し、このプライバシーポリシーに反映することにより通知致します。
		</div><!-- end padding_0_10 -->
	   	<div class="clearfix">&nbsp;</div>
            
        <h2 class="sub_headline">2.株式会社RESEEDのプライバシーポリシーの考え方が適用される範囲</h2>
	   	<div class="padding_0_10 margin_btm_10">
            ■ 弊社のプライバシーポリシーについての考え方は、会員が弊社のサービスを利用される場合に適用されます。<br>
			■ 会員が弊社のサービスを利用される際に収集される個人情報は、 弊社の個人情報保護についての考え方に従って管理されます。<br>
			■ 弊社の個人情報保護考え方は、 弊社が直接提供されるサービスのみであり、リンク等でつながった他の組織・会社等のサービスは適用範囲外となります。<br>
			■ 弊社のサービスのご利用は、利用者の責任において行われるものとします。<br>
			■ 弊社のホームページ及び当ホームページにリンクが設定されている他のホームページから取得された各種情報の利用によって生じたあらゆる損害に関して、弊社は一切の責任を負いません。<br>
	   	</div><!-- end padding_0_10 -->
	   	<div class="clearfix">&nbsp;</div>
            
        <h2 class="sub_headline">3.株式会社RESEEDの個人情報の収集と利用</h2>
	   	<div class="padding_0_10 margin_btm_10">
            ■ 弊社が個人情報を取り扱う際には、管理責任者を置き、適切な管理を行うとともに、外部への流出防止に努めます。また、外部からの不正アクセスまたは紛失、破壊、改ざん等の危険に対しては、適切かつ合理的なレベルの安全対策を実施し、個人情報の保護に努めます。<br>
			■ 弊社は、個人情報に係るデータベース等へのアクセス権を有する者を限定し、社内においても不正な利用がなされないように厳重に管理いたします。<br>
			■ 弊社は、個人情報の取扱いを外部に委託することがあります。この場合、グループ会社または個人情報を適正に取り扱っていると認められる委託先（以下「業務委託先」といいます）を選定し、委託契約等において、個人情報の管理、秘密保持、再提供の禁止等、個人情報の漏洩等なきよう必要な事項を取り決めるとともに、適切な管理を実施させます。<br>
			■ 弊社は、ご本人の同意を得た場合、および法令により例外として取り扱うことが認められている場合を除き、収集した個人情報について、収集の際に予め明示した目的または公表している利用目的においてのみ利用いたします。
   		</div><!-- end padding_0_10 -->
   		<div class="clearfix">&nbsp;</div>
            
        <h2 class="sub_headline">4. 問い合わせ先</h2>
  		<div class="padding_0_10 margin_btm_10">
			ここに示した個人情報についての考え方についてご不明な点などございましたら、次のアドレスまで電子メールでお問い合わせください。<br>
			個人情報管理担当 : info@reseed-s.com<br>
	   	</div><!-- end padding_0_10 -->
	   	<div class="clearfix">&nbsp;</div>	   
	</div><!-- end col -->
<?php get_footer(); ?>