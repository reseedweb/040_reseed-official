<?php get_header(); ?>
	<!--<div class="reseed_col_l_842"><!-- begin reseed_col_l_842 -->
	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9">
	    <h1 class="headline">スタッフ紹介</h1>
		<div class="padding_0_10">
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<h3 class="sub_headline">都築 人志　<span class="font80 my_col_purple fontnormal">WEBマーケティング部 部長</span></h3>
				</div>
			</div><!-- end row -->
			<div class="row margin_btm_20">
				<div class="col-xs-12 col-md-4">
					<img class="staff_thumbnails img-thumbnail" src="<?php bloginfo('template_url');?>/images/staff_tsuzuki.jpg" alt="都築 人志">
				</div><!-- end col4 -->
				<div class="col-xs-12 col-md-8">
					<div class="panel-group" id="accordion2">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_1">経歴</a></h4>
							</div>
							<div id="collapse_2_1" class="panel-body collapse in">
									<p>NECでNTT向け次世代ネットワーク網の環境開発の一員になる。環境開発終了後、大手保険会社のシステム管理部へ出向。</p>
									<p>全国の支社にあるPCハードウェアの管理とシステムのトラブル対応チームでPCハードウェア、ネットワークのトラブルシューティングにあたる。もともと興味のあったWeb業界へ。</p>
									<p>オリジナルデザインのインターネット通販を主体とした、インテリアデザイン会社に出向。3年間Weｂデザイン、マーケティングの知識を勉強しながらWebサイト制作、自社サイト制作に携わる。</p>
							</div>
						</div><!-- end panel-default -->
						<div class="panel panel-default">
							<div class="panel-heading">
							<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_2">レシードと自分の現在</a></h4>
							</div>
							<div id="collapse_2_2" class="panel-body collapse">
									<p>RESEEDの創業当初から携わり、現在はマーケティング部の統括に。マーケティングはもちろん、サイト制作からネットワーク構築、システム開発まで会社の何でも屋さんとなる。</p>
									<p>今でもたまに、お客様からパソコントラブル等の相談を受けることがある。よりお客様にできることはないか、お客様以上にお客様のことを考えるをモットーに、弊社サービスの向上に務めています。</p>
							</div>
						</div><!-- end panel-default -->
						<div class="panel panel-default">
							<div class="panel-heading">
							<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_3">将来</a></h4>
							</div>
							<div id="collapse_2_3" class="panel-body collapse">
									<p>グローバルな視野を持つ人材教育を徹底し、今ある、RESEEDのサービスを国や文化に合わせて、
											海外に向けて発信していく、また逆に海外で学んだマーケティングを日本で活用し、
											世界で通用する企業としてRESEEDを成長させていき、それとともに自身も成長させていきたいです。</p>
									<p>IT企業としてまだまだやれること、やりたいことがたくさんあるため、今からワクワクしています。</p>
							</div>
						</div><!-- end panel-default -->
					</div><!-- end panel-group -->
				</div><!-- end col8 -->
			</div><!-- end row -->
			<div class="clearfix">&nbsp;</div>
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<h3 class="sub_headline">大森 亨一　<span class="font80 my_col_purple fontnormal">SEMコンサルタント</span></h3>
				</div>
			</div><!-- end row -->
			<div class="row margin_btm_20">
				<div class="col-xs-12 col-md-4">
					<img class="staff_thumbnails img-thumbnail" src="<?php bloginfo('template_url');?>/images/staff_omori.jpg" alt="大森 亨一">
				</div><!-- end col8 -->
				<div class="col-xs-12 col-md-8">
					<div class="panel-group" id="accordion21">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion21" href="#collapse_21_1">経歴</a></h4>
							</div>
							<div id="collapse_21_1" class="panel-body collapse in">
									<p>WebマーケティングやSEM、PPC広告など、お客様のニーズに応えれるよう、日々マーケティング知識の勉強とネットユーザーのニーズを発見しながら コンサルティングに受持している。</p>
							</div>
						</div><!-- end panel-default -->
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion21" href="#collapse_21_2">レシードと自分の現在</a></h4>
							</div>
							<div id="collapse_21_2" class="panel-body collapse">
									<p>レシードでは、webマーケティングについての知識を深めるための成功実績が多くあります。
										成功した要因を追求することにより、それぞれのお客様に合わせたオーダーメイドなコンサルティングが可能と考えています。
										そのコンサルティングを学ぶ環境がレシードでは整っておりweb知識だけでなく、様々な業界知識についても勉強し、知識と経験を増やしています。</p>
							</div>
						</div><!-- end panel-default -->
						<div class="panel panel-default">
							<div class="panel-heading">
							<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion21" href="#collapse_21_3">将来</a></h4>
							</div>
							<div id="collapse_21_3" class="panel-body collapse">
									<p>レシードをグローバル企業にしていくために、会社の収益とお客様のニーズを深めていきます。
										その中で、人を育てる「マネージャー」になりたいと考えています。</p>
							</div>
						</div><!-- end panel-default -->
					</div><!-- end panel-group -->
				</div><!-- end col8 -->
			</div><!-- end row -->
			<div class="clearfix">&nbsp;</div>
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<h3 class="sub_headline">安東 良芙　<span class="font80 my_col_purple fontnormal">SEMコンサルタント</span></h3>
				</div>
			</div><!-- end row -->
			<div class="row margin_btm_20">
				<div class="col-xs-12 col-md-4">
					<img class="staff_thumbnails img-thumbnail" src="<?php bloginfo('template_url');?>/images/staff_ando.jpg" alt="安東 良芙">
				</div><!-- end col4 -->
				<div class="col-xs-12 col-md-8">
					<div class="panel-group" id="accordion22">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion22" href="#collapse_22_1">経歴</a></h4>
							</div>
							<div id="collapse_22_1" class="panel-body collapse in">
									<p>福岡生まれの九州男児。WEB業界に携わって４年目。<br />
									関西圏の飲食・美容・スクール業を中心にホームページ制作を行う。９０社の制作実績あり。</p>
							</div>
						</div><!-- end panel-default -->
						<div class="panel panel-default">
							<div class="panel-heading">
							<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion22" href="#collapse_22_2">レシードと自分の現在</a></h4>
							</div>
							<div id="collapse_22_2" class="panel-body collapse">
									<p>レシードでは独自のWEBノウハウを活用し、「ホームページの効果が出ていないお客様」や「これからホームページを制作しようとしているお客様」に、
										客観的に現状業務をヒアリングして、問題点を指摘し、原因を分析し、企業の発展を助けている会社です。<br />
									その為に日々、マーケティングからSEO・SEMを研究しております。</p>
							</div>
						</div><!-- end panel-default -->
						<div class="panel panel-default">
							<div class="panel-heading">
							<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion22" href="#collapse_22_3">将来</a></h4>
							</div>
							<div id="collapse_22_3" class="panel-body collapse">
									<p>WEBコンサルティング業のスキルを一流にしお客様から「ありがとう」と言ってもらえる様に日々精進していきます。</p>
							</div>
						</div><!-- end panel-default -->
					</div><!-- end panel-group -->
				</div><!-- end col8 -->
			</div><!-- end row -->
			<div class="clearfix">&nbsp;</div>

			<div class="row">
				<div class="col-xs-12 col-md-12">
					<h3 class="sub_headline">佐々木 明日香　<span class="font80 my_col_purple fontnormal">デザイナー</span></h3>
				</div>
			</div><!-- end row -->
			<div class="row margin_btm_20">
				<div class="col-xs-12 col-md-4">
					<img class="staff_thumbnails img-thumbnail" src="<?php bloginfo('template_url');?>/images/staff_sasaki.jpg" alt="佐々木 明日香">
				</div><!-- col4 -->
				<div class="col-xs-12 col-md-8">
					<div class="panel-group" id="accordion4">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapse_4_1">経歴</a></h4>
							</div>
							<div id="collapse_4_1" class="panel-body collapse in">
								<div class="accordion-inner">
									<p>愛知県の豊橋市に生まれ、山と川の自然に恵まれた環境で育った。</p>
									<p>幼い頃から音楽と絵を描くのが好きな女の子だった。そのまま大きくなった私は、2008年に大阪芸術大学のデザイン学科に進学。
										グラフィックデザインとイラストレーションを４年間学ぶ。</p>
									<p>大学卒業後、Webデザインに興味を持ち、デジタルハリウッドのWebデザイナーコースを修学。Webデザインを基礎から学んだ。
										そして、2012年の冬からレシードでWebデザイナー&ディレクターとして務めている。</p>
								</div>
							</div>
						</div><!-- end panel-default -->
						<div class="panel panel-default">
							<div class="panel-heading">
							<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapse_4_2">レシードと自分の現在</a></h4>
							</div>
							<div id="collapse_4_2" class="panel-body collapse">
								<div class="accordion-inner">
									<p>2013年初夏現在、私がレシードで働き始めて約半年になるのですが、体感でいうと１年以上長く務めているような気がしています。</p>
									<p>それだけ、毎日が充実していて濃いお仕事を任せられている。</p>
									<p>ただのWebデザイナーではなく、マーケティングやSEO、経営の戦略まで見通せるようなWebマスターになるべく、精勤する毎日。</p>
								</div>
							</div>
						</div><!-- end panel-default -->
						<div class="panel panel-default">
							<div class="panel-heading">
							<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapse_4_3">将来</a></h4>
							</div>
							<div id="collapse_4_3" class="panel-body collapse">
								<div class="accordion-inner">
									<p>私がレシードに務めて実感したことは、『Webも制作も人と人とのコミュニケーションが１番重要』だということだった。</p>
									<p>ネット画面の向こう側で顔が見えないからこそ、想像力をフルに働かせてユーダビリティを思案しなければ、人を引きつけるWebサービスは生まれない。</p>
									<p>それを忘れず、これからも常に新しいWebサービスやデザインの制作に臨んでいく。</p>
								</div>
							</div>
						</div><!-- end panel-default -->
					</div><!-- end panel-group -->
				</div><!-- col4 -->
			</div><!-- end row -->
			<div class="clearfix">&nbsp;</div>

			<div class="row">
				<div class="col-xs-12 col-md-12">
					<h3 class="sub_headline">筒井 亮　<span class="font80 my_col_purple fontnormal">デザイナー</span></h3>
				</div>
			</div><!-- end row -->
			<div class="row margin_btm_20">
				<div class="col-xs-12 col-md-4">
					<img class="staff_thumbnails img-thumbnail" src="<?php bloginfo('template_url');?>/images/staff_tsutsui.jpg" alt="筒井　亮">
				</div><!-- end col4 -->
				<div class="col-xs-12 col-md-8">
					<div class="panel-group" id="accordion6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion6" href="#collapse_6_1">経歴</a></h4>
							</div>
							<div id="collapse_6_1" class="panel-body collapse in">
								<div class="accordion-inner">
									<p>大学ではシステムエンジニアを志望していましたがデザインにも興味があったので両方出来る道はと探した結果Webデザイナーという職に行き当たった。
											そして社会人二年目の時にデジタルハリウッドのWebデザイナー専攻を修学。</p>
									<p>Adobeソフトやデザインの勉強をし同じ目標をもった仲間とも出会え、とても良い経験を詰める。</p>
									<p>それから転職先を探していた時に今の社長とお話させていただく機会がありレシードの仕事と、社長の考えに興味が湧き入社に至る。</p>
								</div>
							</div>
						</div><!-- end panel-default -->
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion6" href="#collapse_6_2">レシードと自分の現在</a></h4>
							</div>
							<div id="collapse_6_2" class="panel-body collapse">
								<div class="accordion-inner">
									<p>2013年5月に入社後、Webデザインチームに配属。</p>
									<p>現在はクライアント案件でディレクションからホームページデザイン、コーディング。</p>
									<p>CMSの導入を行っている。またマーケティング、リスティングなどの経験もこれから学んでいきたいと思っている。</p>
									<p>また、ロープレ、ワークフロー構築、マニュアル化など毎日が学びに満ちている。
											自分のためにしていることが実感出来るので今はとても仕事に充実している。</p>
								</div>
							</div>
						</div><!-- end panel-default -->
						<div class="panel panel-default">
							<div class="panel-heading">
							<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion6" href="#collapse_6_3">将来</a></h4>
							</div>
							<div id="collapse_6_3" class="panel-body collapse">
								<div class="accordion-inner">
									<p>4年後に憧れていたフリーランスとして独立を考えている。</p>
									<p>デザインやコーディングだけではなく本気でマーケティング、ディレクションなどの経験を積める場所がレシードだと思う。
													これからもクライアント視点にたったWebサービス、デザインを制作していくよう務めたいと思っている。</p>
									<p>レシードにとっても私にとっても互いに成長できることを目指して学び、貢献していく。</p>
								</div>
							</div>
						</div><!-- end panel-default -->
					</div><!-- end panel-group -->
				</div><!-- end col8 -->
			</div><!-- end row -->
			<div class="clearfix">&nbsp;</div>
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<h3 class="sub_headline">北岡 佳奈子　<span class="font80 my_col_purple fontnormal">デザイナー</span></h3>
				</div>
			</div><!-- end row -->
			<div class="row margin_btm_20">
				<div class="col-xs-12 col-md-4">
					<img class="staff_thumbnails img-thumbnail" src="<?php bloginfo('template_url');?>/images/staff_kitaoka.jpg" alt="北岡　佳奈子">
				</div><!-- end col4 -->
				<div class="col-xs-12 col-md-8">
					<div class="panel-group" id="accordion7">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion7" href="#collapse_7_1">経歴</a></h4>
							</div>
							<div id="collapse_7_1" class="panel-body collapse in">
								<div class="accordion-inner">
										<p>１８歳で大阪に出てくるまでは、鳥取県で自然に囲まれて青春時代を過ごしていました。<br>
										  大学では放送や広告分野の勉強をし、ドラマやCM制作をすることもありました。卒業後のことを考えた時にWEBデザイナーという職業に出会い興味を持ち、卒業後は新たにWEBデザインの学校で勉強を始めました。<br>
										WEBデザインの学校に通うなかレシードと出会い、現在に至ります。</p>
								</div>
							</div>
						</div><!-- end panel-default -->
						<div class="panel panel-default">
							<div class="panel-heading">
							<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion7" href="#collapse_7_2">レシードと自分の現在</a></h4>
							</div>
							<div id="collapse_7_2" class="panel-body collapse">
								<div class="accordion-inner">
									   <p>レシードでWEBデザイナーとして働きはじめ約半年が経ちました。<br>
										主にウェブサイト制作をしていますが、それだけでなく名刺作成やパンフレットなどの販促物関係の製作に携わる機会もあり、様々な形でお客さまに関わる中で自分の知識や技術の未熟さに悶々としながらも、精進の日々を送っております。</p>
								 </div>
							</div>
						</div><!-- end panel-default -->
						<div class="panel panel-default">
							<div class="panel-heading">
							<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion7" href="#collapse_7_3">将来</a></h4>
							</div>
							<div id="collapse_7_3" class="panel-body collapse">
								<div class="accordion-inner">
											<p>まずは、ひとつひとつの案件を確実にこなせるWEBデザイナーもなることです。<br>
											  クライアントのお客様の希望や、エンドユーザーであるクライアント企業のお客さまの望んでいることを取り入れ、<br>
											  利益につながるサイトの制作が出来る様になりたいと思っています。</p>
								</div>
							</div>
						</div><!-- end panel-default -->
					</div><!-- end panel-group -->
				</div><!-- end col8 -->
			</div><!-- end row -->
			<div class="clearfix">&nbsp;</div>
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<h3 class="sub_headline">ベセディン ドミトリ　<span class="font80 my_col_purple fontnormal">デザイナー</span></h3>
				</div>
			</div><!-- end row -->
			<div class="row margin_btm_20">
				<div class="col-xs-12 col-md-4">
					<img class="staff_thumbnails img-thumbnail" src="<?php bloginfo('template_url');?>/images/staff_domi.jpg" alt="ドミ">
				</div><!-- end col4 -->
				<div class="col-xs-12 col-md-8">
					<div class="panel-group" id="accordion8">
						<div class="panel panel-default">
							<div class="panel-heading">
							<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion8" href="#collapse_8_1">経歴</a></h4>
							</div>
							<div id="collapse_8_1" class="panel-body collapse in">
								<div class="accordion-inner">
									<p>2006年日本の魅力に引かれ日本に移住。</p>
									<p>複数のオンラインショップ・ブランドサイトの立ち上げ、コンセプト企画・運用、UI構築に関わる実務経験を持っている。</p>
									<p>HTML5などの技術を使用したリッチ・サイト＆ウェブアプリケーションをつくることが強みである。</p>
								</div>
							</div>
						</div><!-- end panel-default -->
						<div class="panel panel-default">
							<div class="panel-heading">
							<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion8" href="#collapse_8_2">レシードと自分の現在</a></h4>
							</div>
							<div id="collapse_8_2" class="panel-body collapse">
								<div class="accordion-inner">
									   <p>2013年6月に入社。</p>
									   <p>レシードで仕事をしていく上で、常に心がけている事は使いやすさ、オリジナリティ、クリエイティブそしてクライアントにとって完璧な商品を作ること。</p>
									   <p>学生時代は、ボクシングをやっていたのだが、卒業してから、リング上にいる時の気持ちを味わっていないというスパンがかなり長かった。
										入社してから、自分自身はまだまだ伸びると思い、伸びないと意味が無いと改めて確信した。 </p>
										<p>僕にとって、レシードは、闘志や戦意をもって向かうべきリングの様な大好きな環境だと思っている。</p>
								</div>
							</div>
						</div><!-- end panel-default -->
						<div class="panel panel-default">
							<div class="panel-heading">
							<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion8" href="#collapse_8_3">将来</a></h4>
							</div>
							<div id="collapse_8_3" class="panel-body collapse">
								<div class="accordion-inner">
									<p>配属している事業部のサービスを、収益という面で成功させること。 </p>
									<p>デザインチームが持つリソースを最大限に活かしながらクライアントに喜んでもらえるような新サービスを作り上げたい。
										その結果として収益性が飛躍的に向上し、スタッフの一員として、レシードをより大きな影響力を持つ会社にしたい。</p>
									<p>また、僕が主体となって、新事業を立ち上げ収益化させたいことを、夢にしている。</p>
								</div>
							</div>
						</div><!-- end panel-default -->
					</div><!-- end panel-group -->
				</div><!-- col8-->
			</div><!-- end row -->
		</div><!-- end padding_0_10 -->
		<div class="clearfix">&nbsp;</div>
	</div><!-- end col -->
<?php get_footer(); ?>