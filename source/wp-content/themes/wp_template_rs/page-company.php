<?php get_header(); ?>
	<!--<div class="reseed_col_l_842" itemscope itemtype="http://schema.org/Organization"><!-- begin reseed_col_l_842 -->
	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9" itemscope itemtype="http://schema.org/Organization">
	   <h1 class="headline">企業情報</h1>
		<div class="padding_0_10"><!-- begin padding_0_10 -->
	   		<h2 class="sub_headline">代表挨拶</h2>
	   		<div class="padding_0_10 margin_btm_10">
	   			<div class="row">
					  <div class="col-xs-12 col-md-4">
					  	<img id="ceo_img" class="img-thumbnail media-object margin_right20 margin_btm_20" src="<?php bloginfo('template_url');?>/images/staff_ohe.jpg" alt="大江 栄年" />
					  </div>
					  <div class="col-xs-12 col-md-8" itemscope itemtype="http://schema.org/Person">
					  	<p>弊社はデジタル分野に特化したWEBマーケティングから、グローバル展開におけるWEBビジネスの収益向上支援を行うグローバルマーケティングまで、マーケティング力とシステム構築を中心に行う企業でございます。 昨今の市場では、広告費の高騰、競合の多様性により、ノウハウや知識がなければ費用対効果の高いマーケティング施策を行うことや、システマチックな思考などを元に成果を得ることが難しくなってきております。</p>
					  	<p>弊社はマーケティングとシステム構築には費用対効果を一番に最重要視しております。</p>
					  	<p>PDCAを繰り返し無駄な予算を削減し利益率を向上させるノウハウを独自に蓄えており、多くの中小企業様に継続的なサービスをご利用頂いております。</p>
					  	<p>今後も多くの企業様のサポートをすることと、最新のIT技術を駆使することで高品質のサポートと戦略構築のサービスをご提供し続けて参ります。</p>
					  	<p>2012年度からベトナムを中心とする東南アジア地域の進出、2013年には中国市場にも参入を行っており、1億2000万の国内マーケットもしかり、63億の海外マーケットを基本市場として戦略的に開拓を進めて参ります。</p>
					  	<p class="fontbold margin_top_20"><strong><span itemprop="jobTitle">代表取締役</span>　<span itemprop="name">大江 栄年</span></strong></p>
					</div>
			  </div>
	   		</div><!-- end padding_0_10 -->
	   		<div class="clearfix">&nbsp;</div>

	   		<h2 class="sub_headline">会社概要</h2>
	   		<div class="padding_0_10">
	   			<div class="col-xs-12">
	   				<table id="comp_outline" class="table" width="100%">
			     		<tr>
			     			<th width="25%" class="vert_middle">商号</th>
			     			<td itemprop="name">株式会社レシード 【reseed.co,ltd】</td>
			     		</tr>
			     		<tr>
			     			<th>所在地</th>
			     			<td>
		     				  <ul class="list-unstyled margin_none" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				     				<li>【本社】</li>
				     				<li>〒<span itemprop="postalCode">532-0011</span>
				     					<span itemprop="addressLocality">大阪市淀川区西中島</span><span itemprop="streetAddress">5-6-9</span> 新大阪第一ビル 3階</li>
				     				<li>Mail： info@Treseed-s.com</li>
		     				  </ul>
                              <ul class="list-unstyled margin_none" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				     				<li>【東京営業所】</li>
				     				<li>〒<span itemprop="postalCode">105-0021</span>
				     					<span itemprop="addressLocality">東京都港区東新橋</span><span itemprop="streetAddress">1-10-1</span> 東京ツインパークス レフトウイング</li>
		     				  </ul>
			     				<ul class="list-unstyled margin_none">
				     				<li>【物流・配送センター】</li>
				     				<li>〒556-0001 大阪市浪速区下寺2-3-3</li>
			     				</ul>
	     				  <ul class="list-unstyled margin_none">
				     				<li>【ベトナム開発センター】</li>
				     				<li>Jabes building 7th floor, 110 Cach Mang Thang Tam, Ward7, Dist3, Ho Chi Minh City, Vietnam</li>
			     				</ul>
		     			  </td>
			     		</tr>
			     		<tr>
			     			<th class="vert_middle">連絡先</th>
			     			<td>
			     				<ul class="list-unstyled margin_none">
			     				<li>【本社】</li>
				     				<li>Tel：  <span itemprop="telephone">06-4862-4164</span></li>
				     				<li>Fax： <span itemprop="faxNumber">06-4862-4174</span></li>
				     				<li>Mail： <span itemprop="email">info@reseed-s.com</span></li>
			     				</ul>
			     				<ul class="list-unstyled margin_none">
			     				<li>【東京営業所】</li>
				     				<li>Tel：  <span itemprop="telephone">03-5537-5816</span></li>
				     				<li>Fax： <span itemprop="faxNumber">03-5537-5817</span></li>
			     				</ul>
			     			</td>
			     		</tr>
			     		<tr>
			     			<th>代表取締役</th>
			     			<td>大江　栄年</td>
			     		</tr>
			     		<tr>
			     			<th>資本金</th>
			     			<td>4,000,000円</td>
			     		</tr>
			     		<tr>
			     			<th>設立</th>
			     			<td>平成23年12月26日</td>
			     		</tr>
			     		<tr>
			     			<th>事業内容</th>
			     			<td>
			     				<ul class="list-unstyled margin_none">
			     					<li>■ 国内事業</li>
			     					<li>WEBインテグレーション事業</li>
			     					<li>SEM総合マーケティング事業</li>
			     					<li>EC収益化戦略構築プラットフォーム事業</li>
			     				</ul>
			     				<ul class="list-unstyled margin_none">
			     					<li>■ 海外事業＜グローバルマーケティング＞</li>
			     					<li>SPプラットホーム事業</li>
			     					<li>システム、デザインオフショア開発</li>
			     					<li>アジア地域マーケティング</li>
                                    <li>貿易、仕入代行</li>
                                    <li>※弊社スタッフ約半数は、英語・中国語など多言語対応が可能です</li>
			     				</ul>
			     			</td>
			     		</tr>
			     		<tr>
			     			<th>従業員数</th>
			     			<td>32名（アルバイト、契約社員、パート含まず）</td>
						</tr>
			     		<tr>
			     			<th>取引銀行</th>
			     			<td>三井住友新大阪支店　楽天銀行</td>
			     		</tr>
			     		<tr>
			     			<th>取引先</th>
			     			<td>
			     				<ul class="list-unstyled margin_none">
			     					<li>ヤフー株式会社</li>
			     					<li>グーグル株式会社</li>
			     					<li>GMOインターネット株式会社</li>
			     					<li>GMOクラウド株式会社</li>
			     					<li>GMOデジタルラボ株式会社</li>
			     					<li>GEEKS株式会社</li>
			     					<li>日本システム収納株式会社</li>
			     					<li>ソフトバンク株式会社</li>
			     					<li>イーモバイル株式会社</li>
			     					<li>株式会社クレディセゾン</li>
		     					    <li>フェイスブック株式会社</li>
			     					<li>他約100社</li>
			     				</ul>
		     			  </td>
						</tr>
						<tr>
						  <td>顧問</td>
							<td>
							<ul class="list-unstyled margin_none">
								<li>弁護士法人咲くやこの花法律事務所</li>
								<li>曽我部税理士会計事務所</li>
								<li>KES社労士事務所</li>
							</ul>
							</td>
						</tr>
						<tr>
							<td>参加団体</td>
							<td>パッションリーダーズ</td>
						</tr>
						<tr>
							<td>会社研修機関</td>
							<td>グロービス経営大学大学院「MBA研修」</td>
						</tr>						
			     		<tr>
			     			<th width="25%">アクセスマップ</th>
			     			<td><p>【本社】</p>
			     				<div id="map_canvas1"></div>
                                <p>【東京営業所】</p>
			     				<div id="map_canvas3"></div>
			     				<p>【物流・配送センター】</p>
			     				<div id="map_canvas2"></div>
			     			</td>
			     		</tr>
			     	</table>
			     	<!-- 
			     	<div class="row">
			     		<div class="col-xs-12 col-md-6">
				     		<div class="padding_0_10">
				     			<img class="img-thumbnail" src="images/bld1.jpg" alt="Studio 新大阪、入口" />
				     		</div>
				     	</div>
				     	<div class="col-xs-12 col-md-6">
				     		<div class="padding_0_10">
				     			<img class="img-thumbnail" src="images/bld2.jpg" alt="Studio 新大阪、ロビー" />
				     		</div>
				     	</div>
			     	</div>
			     	 -->					
				</div><!-- end col-xs-12 -->
	   		</div><!--  end padding_0_10 -->
	   		<div class="clearfix margin_top_10">&nbsp;</div>
	     	<h2 class="headline">沿革</h2>

	   		<div class="padding_0_10">
	   			<table class="table table-striped" width="100%">
	     			<tr>
	     				<th>2011年12月</dt>
	     				<td>株式会社レシード設立</td>
	     			</tr>
	     			<tr>
	     				<th>2012年4月</dt>
	     				<td>株式会社レシード営業開始<br />
                        WEBマーケティング事業開始</td>
	     			</tr>
	     			<tr>
	     				<th>2012年5月</dt>
	     				<td>役員を含む人員2名にて、 中国・ベトナムを中心とする企業との取引開始</td>
	     			</tr>
	     			<tr>
	     				<th>2012年6月</dt>
     				  <td>グーグルオープンビジネスパートナー認定<br />
	     				ヤフーリスティング代理店認定</td>
	     			</tr>
	     			<tr>
	     				<th width="30%">2012年12月</dt>
	     				<td>正社員7名に増員</td>
	     			</tr>
	     			<tr>
	     				<th>2013年2月</dt>
	     				<td>ステュディオ新大阪816にオフィス移転</td>
	     			</tr>
	     			<tr>
	     				<th>2013年5月</dt>
	     				<td>正社員9名に増員<br />
                        月次で海外出張、及びマーケティング調査開始<br />
                        クライアント数100社突破</td>
	     			</tr>
	     			<tr>
	     				<th>2013年7月</dt>
	     				<td>大阪市浪速区にて物流・配送センターを設立<br />
                        月間予算1000万突破、営業利益30%達成</td>
	     			</tr>
	     			<tr>
	     				<th>2013年9月</dt>
	     				<td>従業員14名に増員<br />
                        クライアント数500社突破</td>
	     			</tr>
	     			<tr>
	     				<th>2013年10月</dt>
	     				<td>月間予算2000万突破、営業利益20%達成</td>
	     			</tr>
	     			<tr>
	     				<th>2013年12月</dt>
	     				<td>ベトナムオフィス設立<br />
                        ベトナム現地スタッフ3名採用</td>
	     			</tr>
	     			<tr>
	     				<th>2014年1月</dt>
	     				<td>ベトナムオフィス営業開始<br />
                        従業員18名に増員</td>
	     			</tr>
	     			<tr>
	     				<th>2014年6月</dt>
	     				<td>従業員23名に増員</td>
	     			</tr>
	     		</table>
	   		</div><!-- end padding_0_10 -->
	   </div><!-- end padding_0_10 -->
	</div><!-- end reseed_col_l_842 -->	
	<script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
	<script type="text/javascript">
		function initialize() {
			var map_canva1 = document.getElementById('map_canvas1');
			var map_options1 = {
			  center: new google.maps.LatLng(34.7296057,135.4998928),
			  zoom: 14,
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			};


			var map = new google.maps.Map(map_canvas1, map_options1);

			var latlng = new google.maps.LatLng(34.7296057,135.4998928);
			var marker = new google.maps.Marker({
			  position: latlng,
			  map: map
			});

			var map_canvas2 = document.getElementById('map_canvas2');
			var map_options2 = {
			  center: new google.maps.LatLng(34.661785,135.509939),
			  zoom: 14,
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			};


			var map = new google.maps.Map(map_canvas2, map_options2);

			var latlng = new google.maps.LatLng(34.661785,135.509939);
			var marker = new google.maps.Marker({
			  position: latlng,
			  map: map
			});
			
			
			var map_canvas3 = document.getElementById('map_canvas3');
			var map_options3 = {
			  center: new google.maps.LatLng(35.661468, 139.759916),
			  zoom: 14,
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			};

			var map = new google.maps.Map(map_canvas3, map_options3);

			var latlng = new google.maps.LatLng(35.661468, 139.759916);
			var marker = new google.maps.Marker({
			  position: latlng,
			  map: map
			});
		  }
		google.maps.event.addDomListener(window, 'load', initialize);
	</script>
<?php get_footer(); ?>