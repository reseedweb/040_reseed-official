				</div><!-- end left column -->				
				<aside id="right_column">
					<?php if(is_page('blog') || is_category() || is_single()) : ?>
					<?php
						$queried_object = get_queried_object();                                
						$sidebar_part = 'blog';
						if(is_tax() || is_archive()){                                    
							$sidebar_part = '';
						}                               
						if($queried_object->post_type != 'post' && $queried_object->post_type != 'page'){
							$sidebar_part = '';
						}   
						if($queried_object->taxonomy == 'category'){                                    
							$sidebar_part = 'blog';
						}                 
					?>
					<?php get_template_part('sidebar',$sidebar_part); ?>  
					<?php else: ?>
						<?php get_template_part('sidebar'); ?>  
					<?php endif; ?>                                    
				</aside><!-- end right column -->
			</div><!-- end row -->
        </div><!-- end container -->
		<div class="clearfix">&nbsp; </div>
				
		<footer id="footer"><!-- begin footer -->
			<div class="container"><!-- begin container -->
				<div class="row">
					<div class="hidden-xs col-sm-3 col-md-3 col-lg-3">
						<a href="<?php bloginfo('url');?>/" title="株式会社レシード"><img class="mousehover" src="<?php bloginfo('template_url');?>/images/footer_logo.jpg" alt="株式会社レシード" /></a>
					</div>
					<div id="footer_block1" class="col-xs-12 col-sm-5 col-md-5 col-lg-3">
						<p class="side_tel"><a href="tel:0648624164"><i class="icon-mobile-phone"></i> <span itemprop="telephone"><strong>06-4862-4164</strong></span></a></p>
						<p class="font90">受付： 平日 9:00 - 18:00</p>
					</div>
					<div class="hidden-xs col-sm-3 col-md-3 col-lg-6">&nbsp;</div>
				</div><!-- end row -->
				
				<div class="row hidden-xs">
					<div class="col-xs-12 col-sm-first col-md-3 col-lg-3  margin_top_30">
						 <div id="footer_block2" class="margin_top_20 footer_line_ver" itemscope itemtype="http://schema.org/Organization">
							<p class="font90">&copy; 2013　<span itemprop="name">株式会社レシード</span></p>
							<address class="font80" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
								<p>〒<span itemprop="postalCode">532-0011</span>
								<span itemprop="addressLocality">大阪市</span><span itemprop="streetAddress">淀川区<br/>西中島5-6-9</span>新大阪第一ビル3F</p>
								<a href="https://goo.gl/maps/J3CFl" target="_blank" title="Google地図で見る"><img class="mousehover" src="<?php bloginfo('template_url');?>/images/map_btn.jpg" alt="マップで見る" /></a>
							</address>
						</div>
					</div>					 

					<div class="col-sm-second col-md-3 col-lg-3 margin_top_30">
						<div class="margin_top_20 footer_bread footer_line_ver" itemprop="breadcrumb">
							<a href="<?php bloginfo('url');?>/webmarketing" title="ウェブマーケティング事業紹介">ウェブマーケティング事業</a><br/>
							<a href="<?php bloginfo('url');?>/globalmarketing" title="グローバルマーケティング事業紹介">グローバルマーケティング事業</a>
						</div>
					</div>
					<div class="hidden-xs col-sm-third col-md-3 col-lg-3 margin_top_30">
						<div class="margin_top_20 footer_bread footer_line_ver" itemprop="breadcrumb">
							<a href="<?php bloginfo('url');?>/staff" title="社員紹介ページへ">社員紹介</a><br/>
							<a href="<?php bloginfo('url');?>/recruit" title="採用情報に詳しく">採用情報</a>
						</div>
					</div>
					<div class="hidden-xs col-sm-fourth col-md-3 col-lg-3 margin_top_30">
						 <div class="margin_top_20 footer_bread" itemprop="breadcrumb">
							<a href="<?php bloginfo('url');?>/company" title="会社概要ページへ">会社概要</a><br/>
							<a href="<?php bloginfo('url');?>/privacy" title="プライバシーポリシー">プライバシーポリシー</a><br/>
							<a href="<?php bloginfo('url');?>/contact" title="お問い合わせフォームへ移動">お問い合わせ</a>
						</div>
					</div>
				</div><!-- end row -->	
				
				<div class="row visible-xs">
					<div id="footer_block2" class="col-xs-12" itemscope itemtype="http://schema.org/Organization">
						<p class="font90">&copy; 2013　<span itemprop="name">株式会社レシード</span></p>
						<address class="font80" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
							<p>〒 <span itemprop="postalCode">532-0011</span>、<span itemprop="addressLocality">大阪市</span>
							<span itemprop="streetAddress">淀川区<br/>西中島5丁目6-9</span>新大阪第一ビル3F</p>
							<a href="https://goo.gl/maps/J3CFl" target="_blank" title="Google地図で見る"><img class="mousehover" src="<?php bloginfo('template_url');?>/images/map_btn.jpg" alt="マップで見る" /></a>
						</address>
					</div>
				</div><!-- end row -->	
			</div><!-- end container -->
		</footer><!-- end footer -->		
	<?php wp_footer();?>
	</body>
</html>    