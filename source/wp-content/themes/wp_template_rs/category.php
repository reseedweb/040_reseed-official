<?php get_header();?>
<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9">
    <div id="category1"><!-- begin category1 -->
        <h2 class="headline"><?php single_cat_title('',true); ?></h2>
       <?php
		$queried_object = get_queried_object();
		$term_id = $queried_object->term_id;
		//print_r($queried_object);
		?>
		
		<?php
			$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
			$posts = get_posts(array(
			'post_type'=> 'post',
			'posts_per_page' => 5,
			'paged' => $paged,
				'tax_query' => array(
				array(
				'taxonomy' => $queried_object->taxonomy,
				'field' => 'term_id',
				'terms' => $term_id))
		));
		?>   
        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>    
            <!-- do stuff ... -->
            <div class="mt20">
                <h3 class="blog-title"><?php the_title(); ?></h3>            
                <div class="post-row-content clearfix">                
                    <div class="post-row-meta">
                        <i class="fa fa-clock-o"></i><?php the_time('l, F jS, Y'); ?>
                        <i class="fa fa-tags"></i><span class="blog-catelogy"><?php the_category(' , ', get_the_id()); ?></span>
                        <i class="fa fa-user"></i><span class="blog-author"><?php the_author_link(); ?></span>
                    </div><!-- end post-row-meta -->
                    <div class="post-row-description">
						<?php the_excerpt(); ?>
					</div><!-- end post-row-description -->
                    <div class="blog-btnrm">
						<a href="<?php the_permalink(); ?>">Read more</a>
					</div>               
                </div><!-- end post-row-content -->
            </div>
            <?php endwhile; ?>    
            <div class="mt20">
                <?php if(function_exists('wp_pagenavi')) wp_pagenavi(); ?>
            </div>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
    </div><!-- end category1 -->
</div><!-- end col -->
<?php get_footer();