<?php get_header();?>
<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9">
	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
	<h2 class="headline"><?php the_title(); ?></h2>
	<div class="post-row-content clearfix">
		<div class="post-row-meta">
			<i class="fa fa-clock-o"></i><?php the_time('l, F jS, Y'); ?>
			<i class="fa fa-tags"></i><span class="blog-catelogy"><?php the_category(' , ', get_the_id()); ?></span>
            <i class="fa fa-user"></i><span class="blog-author"><?php the_author_link(); ?></span>
		</div>    
    	<div class="blog-content"><?php the_content(); ?></div>
    </div><!-- end post-row-content -->   
	<?php endwhile; endif; ?>
    <!-- post navigation -->
    <div class="navigation">
		<?php if( get_previous_post() ): ?>
		<div class="single-blog-l"><?php previous_post_link('%link', '« %title'); ?></div>
		<?php endif;
		if( get_next_post() ): ?>
		<div class="single-blog-r"><?php next_post_link('%link', '%title »'); ?></div>
		<?php endif; ?>
    </div>
    <!-- /post navigation -->	
</div>
<?php get_footer();?>