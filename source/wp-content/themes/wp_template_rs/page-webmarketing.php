<?php get_header(); ?>
	<!--<div class="reseed_col_l_842"><!-- begin reseed_col_l_842 -->
	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9">	   
		<h1 class="headline">ウェブマーケティング事業</h1>
		<div class="padding_0_10"><!-- begin padding_0_10 -->
	   		<p>デジタル化＆ネットワーク化により、日々大幅に拡大しているデータを本格的に活用していくためには、「見える化」することが必要。<br />
	   			そして、「見える化」されたデータを、効果のあるマーケティング施策に落とし込んで、それを社内体制及び業務フローまで定着させていくことは、競争に勝つために欠かせない条件である。<br />
                RESEEDはROIを最大化するために、継続的にストックしたログ解析データをもとに、マーケティング分析、及びサイト開発や最適化サービスを行っております。
	   			定期運用サービスをはじめ、クライアントにおける社内スタッフのスキルアップ育成から、スタッフ育成の環境体制づくりに関する部分まで包括的にコンサルティングサービスをご提供することで、クライアントのビジネス拡大に貢献します。
	   		</p>
	   		<div height="30">&nbsp;</div>
	   		<h2 class="sub_headline">SEMを中心としたコンサルティング | 自社メディアサービス</h2>
	   		<div class="row">
				<div class="col-xs-12 col-md-8">
					<h3 class="media-heading fontbold my_col_blue">SEMコンサル.com</h3>
					<p class="margin_ver10"><span class="media_link">サイトURL</span> <a href="http://sem-consul.com/" alt="SEMコンサル.com" target="_blank">http://<span class="my_col_midblue">sem-consul.com/</span></a></p>
					<p class="my_col_red fontbold margin_ver10">
						リスティング広告・PPC広告運用のエキスパート集団がWEBサイトにおける<br/>費用対効果を最大化させます。
					</p>
					<p class="margin_ver10">SEMコンサルでは、クライアント様WEBサイトを数多くのユーザーに認知していただくために、
						リスティング広告の掲載及び運用を代行支援するサービスを提供しております 。<br />
						検索エンジンからのユーザーを獲得するだけではなく、WEBサイトの知名度を向上しつつ、売上貢献のために、
						専門の運用担当がサイトの各種コンバージョン率を分析して、ヤフーリスティング広告 / グーグルアドワーズ広告 /
						アドネットワーク広告等の各種広告媒体において、必ず費用対効果の高いキーワードを出稿し、無駄のない予算にてリスティング広告の運用代行を行います。
					</p>
				</div>
				<div class="col-xs-6 col-md-4">
					<img class="media-object margin_left20" src="<?php bloginfo('template_url');?>/images/web_marketing_1.jpg" alt="SEMコンサル" />
				</div>
			</div><!-- end row -->
			
	   		<h3 class="sub_headline_border margin_btm_20">エキスパート集団が保有する独自の強み</h3>
	   		<div class="padding_0_10">
	   			<div class="row">
	   				<div class="col-xs-12 col-md-8">
	   					<p class="my_col_red fontbold margin_btm_10">弊社独自のノウハウが生み出すリスティング広告+WEBマーケティング施策</p>
						<p>販売商材を絞込み、リスティング広告からの導線をシンプルに分かりやすくすることで、サイトへ訪問してきたユーザーを迷わせません。<br />
		   				また、トップページからのサイト流入、クリックされている箇所、サイト流入に至る検索キーワードやバナーボタンの解析を行うことで、
		   				ページ最適化を行い、WEBサイトに訪れたユーザーを直接コンバージョンへと誘導いたします。<br />
		   				さらには御社商材の利益率、効率よく販売できる商材の選定、強みを理解したサイト構築を行うことにより、
		   				無駄な広告費・人件費を削減させるといった、WEBサイトを主軸とした包括的なコンサルティングサービスにて、
		   				御社事業の収益化を全面サポートさせていただきます。</p>
	   				</div>
	   				<div class="col-xs-6 col-md-4">
	   					<img class="media-object margin_left20" src="<?php bloginfo('template_url');?>/images/web_marketing_2.jpg" alt="エキスパート集団が保有する独自の強み" />
	   				</div>
	   			</div><!-- end row -->
	   			<div class="row">
	   				<div class="col-xs-12 col-md-8">
	   					<p class="my_col_red fontbold margin_btm_10">独自に開発したSEM効率最適化アルゴリズムに基づく、<br/>リスティング・ネットワーク広告</p>
						<p>販売商材を絞込み、リスティング広告からの導線をシンプルに分かりやすくすることで、サイトへ訪問してきたユーザーを迷わせません。<br />
		   				また、トップページからのサイト流入、クリックされている箇所、サイト流入に至る検索キーワードやバナーボタンの解析を行うことで、
		   				ページ最適化を行い、WEBサイトに訪れたユーザーを直接コンバージョンへと誘導いたします。<br />
		   				さらには御社商材の利益率、効率よく販売できる商材の選定、強みを理解したサイト構築を行うことにより、
		   				無駄な広告費・人件費を削減させるといった、WEBサイトを主軸とした包括的なコンサルティングサービスにて
		   				御社事業の収益化を全面サポートさせていただきます。</p>
	   				</div>
	   				<div class="col-xs-6 col-md-4">
	   					<img class="media-object margin_left20" src="<?php bloginfo('template_url');?>/images/web_marketing_3.jpg" alt="<?php bloginfo('name');?>" />
	   				</div>
	   			</div><!-- end row -->
	   		</div><!-- end padding_0_10 -->
			
	   		<h3 class="sub_headline_border margin_btm_20">ご提供するサービスライン</h3>
	   		<div class="row">
	   			<div class="col-xs-12 col-md-12">
	   				<div class="padding_0_10">
	   					<p>SEMコンサルでは、下記リスティング広告等の運用代行サービスから、ターゲット層に対する分析・調査まで、
	   						クライアントの課題解決に向けた包括的なコンサルティング・サービスを行っております。</p>
	   					<p class="margin_btm_30"><img src="<?php bloginfo('template_url');?>/images/web_marketing_4.jpg" alt="ご提供するサービスライン" width="100%" /></p>
	   				</div>
	   			</div>
	   		</div><!-- end row -->
			
	   		<h2 class="sub_headline">システム開発・サイト受託制作</h2>
	   		<div class="padding_0_10">
	   			<p>クライアント様のご要望や業務フローについてヒアリングを行ったうえで、「使いやすさ」を重視したシステムの設計・開発を行っております。<br />
	   				プロジェクトにおける要件定義から設計、構築、保守・運用そしてインフラ連携まで、一貫したサービス・ラインナップでサポートいたします。<br />
	   				またサイト受託制作では、お客様のご要望をお伺いし、通販サイトやコーポレートサイト、グローバルサイトといったサイト全般のリニューアルから
	   				コンバージョン率の最適化を図ったランディングページ制作まで、クライアントの課題解決の視点に立ったWEB改善施策をご提案させていただいております。
				</p>
	   			<div class="row">
			   		<div class="col-xs-12 col-md-12">
			   			<p class="margin_btm_30"><img src="<?php bloginfo('template_url');?>/images/web_marketing_5.jpg" alt="システム開発・サイト受託制作" / width="100%" /></p>
			   		</div>
			   	</div>
	   		</div><!-- end padding_0_10 -->
		</div><!-- end padding_0_10 -->	
	</div><!-- end col -->
<?php get_footer(); ?>