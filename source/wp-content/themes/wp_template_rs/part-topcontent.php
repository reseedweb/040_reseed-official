<div id="main_img" class="hidden-xs"><!-- begin main_img -->
	<?php if(is_page('company')) : ?>																				
		<img src="<?php bloginfo('template_url');?>/images/comp_main_img.jpg" alt="企業情報"/>											
	<?php elseif(is_page('staff')) : ?>	
		<img src="<?php bloginfo('template_url');?>/images/staff_main_img.jpg" alt="スタッフ紹介"/>	
	<?php elseif(is_page('recruit')) : ?>	
		<img src="<?php bloginfo('template_url');?>/images/recruit_main_img.jpg" alt="採用情報"/>	
	<?php elseif(is_page('privacy')) : ?>	
		<img src="<?php bloginfo('template_url');?>/images/privacy_main_img.jpg" alt="プライバシーポリシー"/>		
	<?php elseif(is_page('webmarketing')) : ?>	
		<img src="<?php bloginfo('template_url');?>/images/wm_main_img.jpg" alt="ウェブマーケティング事業"/>		
	<?php elseif(is_page('globalmarketing')) : ?>	
		<img src="<?php bloginfo('template_url');?>/images/gm_main_img.jpg" alt="グローバルマーケティング事業"/>	
	<?php elseif(is_page('contact')) : ?>	
		<img src="<?php bloginfo('template_url');?>/images/contact_main_img.jpg" alt="お問い合わせ"/>	
	<?php elseif(is_page('thanks')) : ?>	
		<img src="<?php bloginfo('template_url');?>/images/contact_main_img.jpg" alt="送信されました"/>
	<?php else :?>
		<?php the_title(); ?>
	<?php endif; ?>
</div><!-- end main_img -->