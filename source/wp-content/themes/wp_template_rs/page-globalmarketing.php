<?php get_header(); ?>
	<!--<div class="reseed_col_l_842"><!-- begin reseed_col_l_842 -->
	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9">   		
		<h1 class="headline">グローバルマーケティング事業</h1>
		<div class="padding_0_10">
	   		<p class="margin_btm_30">RESEEDでは英語・中国語言語圏を中心としたグローバルなWEBビジネスを展開するクライアント様の収益向上を支援する事業を展開しております。<br />
	   			アジア市場参入を視野に入れたマーケティング調査から、現地検索エンジンに最適化したオンラインWEBビジネス、
	   			そしてRESEEDが提携するグローバル取引先における輸入輸出貿易代行まで、クライアントのグローバル展開における課題を包括的にマーケティング支援いたします。
			</p>

	   		<h2 class="sub_headline">アジア地域のWEBマーケティング</h2>
	   		<div class="padding_0_10 margin_btm_30">
	   			<div class="row">
					<div class="col-xs-12 col-md-8">
						<p>RESEEDでは中国をはじめ、東南アジア言語に対応したSEMソリューションを提供しています。<br />
							現地における検索エンジンの特徴を調査したうえで現状把握を行い、文化的な背景を意識したオンライン広告を作成し、クライアントのグローバル展開を後押しします。<br />
							具体的にオンライン広告におきましては、弊社が最大の強みとするリスティング広告代行支援サービスにて、アジア各国の現地市場を精査し、独自に開発したSEM効率最適化アルゴリズムに基づくリスティング・ネットワーク施策にて他社にはない、より最適化されたサービスをご提供しております。
						</p>
					</div>
					<div class="col-xs-6 col-md-4">
						<img class="service_thumbnails" src="<?php bloginfo('template_url');?>/images/global_marketing_1.jpg" alt="アジア地域のWEBマーケティング">
					</div>
				</div><!-- end row -->
	   		</div><!-- endpadding_0_10 -->

	   		<h2 class="sub_headline">アジア市場マーケティング調査</h2>
	   		<div class="padding_0_10 margin_btm_30">
	   			<div class="row">
					<div class="col-xs-12 col-md-8">
					  	<p><abbr class="my_col_red" title="ROI(Return On Investment)＝費用対効果">ROI<sup>*</sup></abbr>を最大化するためには、営業目標を認識したうえで、
					  		その達成に向けて各類コンバージョンの最適化が重要です。<br />
					  		RESEEDが提供するマーケティング調査サービスにおいて、アジア市場でのゴール、
					  		そして、それを達成するための「見える化」戦略を行います。</p>
						<p class="my_col_red font80">*ROI(Return On Investment)＝費用対効果</p>
					</div>
					<div class="col-xs-6 col-md-4">
						<img class="service_thumbnails" src="<?php bloginfo('template_url');?>/images/global_marketing_2.jpg" alt="アジア市場マーケティング調査" />
					</div>
				</div><!-- end row -->
	   		</div><!-- end padding_0_10 -->
			
	   		<h2 class="sub_headline">販促物・ノベルティ関連制作代行・消耗品等の輸入輸出貿易代行</h2>
	   		<div class="padding_0_10 margin_btm_30">
	   			<div class="row">
					<div class="col-xs-12 col-md-8">
						<p>クライアントが行うグローバル取引を支援するために、消耗品・販促物の貿易代行サービスをご提供しています。<br />
                            RESEEDは現在、中国をはじめとする100社以上の企業及び工場と取引を行っております。<br />
                             このため、日本国内市場において最も安価で消耗品・販促物を選定し、調達することが可能となっておりますので、是非弊社にご相談ください。</p>
					</div>
					<div class="col-xs-6 col-md-4">
						<img class="service_thumbnails" src="<?php bloginfo('template_url');?>/images/global_marketing_3.jpg" alt="販促物・ノベルティ関連制作代行・消耗品等の輸入輸出貿易代行" />
					</div>
				</div><!-- end row -->
	   		</div><!-- end padding_0_10 -->
	   </div><!-- end padding_0_10 -->	
	</div><!-- end col -->
<?php get_footer(); ?>