<?php get_header(); ?>
<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9">   	
	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>	
		<h1 class="headline margin_btm_20">お問い合わせフォーム</h1>
		<?php echo do_shortcode('[contact-form-7 id="28" title="Japanese-Contact-Form"]') ?>				
	<?php endwhile; endif; ?>
</div>
<?php get_footer(); ?>

