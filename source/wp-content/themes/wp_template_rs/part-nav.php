<nav class="navbar-collapse collapse"><!-- begin navi -->
	<ul class="nav navbar-nav navbar-right">
		<li><a href="<?php bloginfo('url');?>/" title="トップページ">トップページ</a></li>
		<li class="dropdown"><a href="#" title="事業案内リスト" class="dropdown-toggle" data-toggle="dropdown">事業内容 <i class="icon-caret-down"></i></a>
			<ul class="dropdown-menu">
				<li><a href="<?php bloginfo('url');?>/webmarketing" title="ウェブマーケティング事業紹介">ウェブマーケティング事業　<i class="icon-chevron-right"></i></a></li>
				<li><a href="<?php bloginfo('url');?>/globalmarketing" title="グローバルマーケティング事業紹介">グローバルマーケティング事業　<i class="icon-chevron-right"></i></a></li>
			</ul>
		</li>
		<li><a href="<?php bloginfo('url');?>/staff" title="社員紹介ページへ">社員紹介</a></li>
		<li><a href="<?php bloginfo('url');?>/recruit" title="採用情報に詳しく">採用情報</a></li>
		<li><a href="<?php bloginfo('url');?>/company" title="会社概要ページへ">会社概要</a></li>
		<li><a href="<?php bloginfo('url');?>/contact" title="お問い合わせフォームへ移動">お問い合わせ</a></li>
	</ul>
</nav><!-- end navi-collapse -->